section .text

 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:  
        cmp byte[rdi + rax], 0x0
        je .out
        inc rax
        jne .loop
        .out: 
            ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rsi, rdi
    mov rdx, rax
    mov rax, 1 
    mov rdi, 1
    syscall
    ret



; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rsi, rsp
    sub rsp, 32
    mov rax, rdi
    mov r8, 10
    dec rsi
    mov byte[rsi], 0
    xor rdx, rdx
    .loop:
        dec rsi
        div r8
        add dl, `0`
        mov byte[rsi], dl
        xor rdx, rdx
        test rax, rax
        jne .loop
    mov rdi, rsi
    call print_string
    add rsp, 32
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0x0
    jge .pozitive
    neg rdi
    push rdi
    mov rdi, `-`
    call print_char
    pop rdi
    .pozitive:
        call print_uint
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    .loop:
        mov dl, byte[rdi]
        cmp dl, byte[rsi]
        jnz .error
        inc rdi
        inc rsi
        cmp byte[rdi-1], 0
        jnz .loop
        mov rax, 1
        ret
        .error:
        xor rax, rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    push rax
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    test rax, rax
    jz .out
    cmp rax, -1
    jz .out
    pop rax
    ret
    .out:
        pop r8
        ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push r12
    push r13
    push r14
    mov r12, rdi
    mov r13, rsi
    mov r14, rcx
    xor r14, r14
    .next:
        call read_char
        cmp rax, ` `
        jz .next
        cmp rax, `\t`
        jz .next
        cmp rax, `\n`
        jz .next
        jmp .no_spaces


    .loop:
        call read_char
        cmp rax, ` `
        jz .success
        cmp rax, `\t`
        jz .success
        cmp rax, `\n`
        jz .success
        .no_spaces:
            test rax, rax
            jz .success
            mov byte[r12+r14], al
            inc r14
            cmp r14, r13
            jna .loop
    pop r14
    pop r13
    pop r12
    xor rax, rax
    ret

    .success:
        test r14, r14
        jz .out
        mov byte[r12+r14], 0
        .out:
            mov rax, r12
            mov rdx, r14
            pop r14
            pop r13
            pop r12
            ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    push rbx
    xor rax, rax
    xor rdx, rdx
    .loop:
        mov bl, byte[rdi+rdx]
        cmp bl, `0`
        jb .out
        cmp bl, `9`
        ja .out

        mov r8, 10
        push rdx
        mul r8
        pop rdx

        sub bl, `0`
        add al, bl
        inc rdx
        jmp .loop
    .out:
        test rdx, rdx
        jnz .not_zero
        mov rdx, 1
        .not_zero:  
            pop rbx
            ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], `-`
    jnz .positive
    cmp byte[rdi], `+`
    jz .positive_with_plus
    inc rdi
    call parse_uint
    test rdx, rdx
    jz .zero_length
    neg rax
    inc rdx
    ret
    .positive_with_plus:
        inc rdi
        call parse_uint
        test rdx, rdx
        jz .zero_length
        inc rdx
        ret
    .positive:
        call parse_uint
        ret
    .zero_length:
        ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    cmp rax, rdx
    jnb .too_long
    push rax
    inc rax
    .loop:
        mov cl, [rdi]
        mov [rsi], cl
        inc rsi
        inc rdi
        dec rax
        test rax, rax
        jnz .loop
    pop rax
    ret
    .too_long:
        xor rax, rax
        ret
